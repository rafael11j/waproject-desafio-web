export default interface IUser {
  id?: number;
  description: string;
  amount: number;
  price: number;
  createdDate: Date;
  updatedDate?: Date;
}
