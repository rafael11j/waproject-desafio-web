//import Button from '@material-ui/core/Button';
import Toolbar from '../../Layout/Toolbar';
import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import ClearIcon from '@material-ui/icons/Clear';
import api from '../../../services/api';
import { useHistory } from 'react-router-dom';

export default function RequestForm(props: any) {
  const useStyles = makeStyles({
    card: {
      minWidth: 275
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)'
    },
    title: {
      fontSize: 22
    },
    pos: {
      marginBottom: 12
    },
    cardTitle: {
      textAlign: 'center'
    },
    input: {
      width: '100%'
    }
  });
  const classes = useStyles(props);
  const history = useHistory();

  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState('');
  const [price, setPrice] = useState('');

  async function onSubmit(e: any) {
    e.preventDefault();

    api.post('/request', { description, amount, price }).subscribe((data: any) => {
      if (data.id != null) {
        alert('Pedido Cadastrado com sucesso!');
        setDescription('');
        setAmount('');
        setPrice('');
        history.push('/pedidos');
      } else {
        alert('Falha ao Cadastrar Pedido!');
      }
    });
  }

  return (
    <Fragment>
      <Toolbar title='Cadastro de Pedido'></Toolbar>

      <Grid container spacing={3}>
        <Grid item xs></Grid>
        <Grid item xs={6}>
          <Card className={classes.card}>
            <form onSubmit={onSubmit}>
              <CardContent>
                <div className={classes.cardTitle}>
                  <span className={classes.title}>Dados do pedido</span>
                </div>
                <br />

                <TextField
                  label='Descrição'
                  className={classes.input}
                  required
                  value={description}
                  onChange={event => setDescription(event.target.value)}
                />
                <br />
                <br />
                <Grid container spacing={3}>
                  <Grid item xs>
                    <TextField
                      label='Quantidade'
                      className={classes.input}
                      required
                      type='number'
                      value={amount}
                      onChange={event => setAmount(event.target.value)}
                    />
                  </Grid>
                  <Grid item xs>
                    <TextField
                      label='Valor'
                      className={classes.input}
                      required
                      type='number'
                      value={price}
                      onChange={event => setPrice(event.target.value)}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardActions>
                <Button type='submit' variant='contained' color='secondary' startIcon={<SaveIcon />}>
                  Salvar
                </Button>
                <Button type='button' variant='contained' startIcon={<ClearIcon />} href='pedidos'>
                  Cancelar
                </Button>
              </CardActions>
            </form>
          </Card>
        </Grid>
        <Grid item xs></Grid>
      </Grid>
    </Fragment>
  );
}
